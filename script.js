// Ошибки
const ERRORS = {
    telError: 'Номер телефона 10 символов',
    defaultError: 'Заполните поле'
}

const inputAll = document.getElementsByTagName('input')
window.onload = function () {
    // Маска для инпута с телефоном
    const inputPhone = document.querySelectorAll('input[type="tel"]')
    for (input of inputPhone) {
        let phoneMask = IMask(
        input, {
            mask: '+{7} (000) 000-00-00'
        }
    )
    }

    // Первая проверка всех возможных инпутов
    for (input of inputAll) {
        checkValidation (input)
    }
}

// Проверка валидности
function checkValidation (e, type) {
    let validation = e.dataset.status
    let errors = e.closest('.input_row').querySelector('.errors')

    let inputLength
    switch (e.type) {
        case 'tel':
            inputLength = e.value.match(/\d/g)
            if (inputLength != null) {
                inputLength = inputLength.length
            } else {
                inputLength = 0
            }
            break

        default:
            inputLength = e.value.length
            break
    }

    switch (type) {
        case 'focus':
            e.classList.add('focus')
            break
        
        case 'blur':
            switch (e.type) {
                case 'tel':
                    switch (e.required) {
                        case true:
                            if (inputLength == 0) {
                                validation = 'error'
                                errors.innerHTML = ERRORS.telError
                                e.classList.remove('focus')
                            } else if (inputLength < 11) {
                                validation = 'error'
                                errors.innerHTML = ERRORS.telError
                            } else {
                                validation = 'ready'
                                errors.innerHTML = ''
                            }
                            break
                        
                        case false:
                            if (inputLength < 11 && inputLength >= 1) {
                                errors.innerHTML = ERRORS.telError
                            } else if (!e.value) {
                                e.classList.remove('focus')
                                errors.innerHTML = ''
                            } else {
                                errors.innerHTML = ''
                            }
                            break
                        
                        default:
                            break
                    }
                    break

                default:
                    if (!e.value && e.required) {
                        validation = 'error'
                        errors.innerHTML = ERRORS.defaultError
                        e.classList.remove('focus')
                    } else if (e.value) {
                        validation = 'ready'
                    } else {
                        e.classList.remove('focus')
                    }
                    break
            }
            break

        case 'input':
            switch (e.type) {
                case 'tel':
                    if (inputLength == 11) {
                        validation = 'ready'
                        errors.innerHTML = ''
                    } else {
                        validation = 'during'
                    }
                    break

                default:
                    if (inputLength >= 1) {
                        validation = 'ready'
                        errors.innerHTML = ''
                    } else {
                        validation = 'during'
                    }
                    break
            }
            break

        default:
            switch (e.type) {
                case 'tel':
                    if (inputLength == 11) {
                        validation = 'ready'
                        e.classList.add('focus')
                    }
                    break

                default:
                    if (inputLength >= 1) {
                        validation = 'ready'
                        e.classList.add('focus')
                    }
                    break
            }
            break
    }
    
    e.dataset.status = validation
}